#!/bin/bash
set -e

COMMANDS="aws curl git less zip jq bash"

if [ "$1" = "--docker-help" ]; then
	echo "Usage: To run this image you can pass these commands only"
	echo
	echo "  $COMMANDS"
	echo
	echo "If you specifiy anything other than those commands then those arguments will be passed to the 'aws' command by default including options"
	echo
	echo "Examples:"
	echo "  ### runs aws help ###"
	echo "  docker run -it --rm aws-cli help"
	echo 
	echo "  ### Also runs aws help ###"
	echo "  docker run -it --rm aws-cli aws help"
	echo 
	echo "  ### Displays the aws cli version ###"
	echo "  docker run --rm aws-cli --version"
	echo 
	echo "  ### Passes arguments to aws cli calling the elasticbeanstalk subcommand ###"
	echo "  docker run -it --rm aws-cli elasticbeanstalk help"
	echo 
	echo "  ### runs an interactive bash shell ###"
	echo "  docker run -it --rm aws-cli bash"
	echo
	echo "  ### zips the data dir ###"
	echo "  docker run --rm aws-cli zip -9 -r pwd.zip ."
	echo
	exit
fi

# if command is one of these then pass control to it with exec
for COMMAND in $COMMANDS
do
	if [ "$1" = "$COMMAND" ]; then
		exec "$@"
	fi
done

# if it's none of the above, then prepend aws
set -- aws "$@"
exec "$@"
