AWS CLI Docker Image
==================
Simple docker image to run aws cli with everything installed and needed to run the aws cli.

Current version is aws-cli/1.8.3 see http://docs.aws.amazon.com/cli/latest/reference/

## Build
```
root:~$ docker build -t aws-cli:latest .
```

## Details
The image is built from the offical python image, currently python:3.4.3. It also includes the following commands accessible via the _docker-entrypoint.sh_ script

- aws 
- curl 
- git 
- less 
- zip 
- jq 
- bash

The image also defines the volumes _/root/.aws_ and _/root/data_ . These volumes can be used to mount credentials and files or scripts to run.

## Usage

By default all commands ( with exception see [Help](#help) ) are passed to the _aws_ command, and the current directory is the volume _/root/data_
The aws cli requires you to pass your credentials to the command. You can achieve this in the following ways.

#### Using environment variables

```
root:~$ docker run -it --rm -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_KEY aws-cli help
```

#### Mounting the aws cli config into the image

```
root:~$ docker run -it --rm -v $HOME/.aws:/root/.aws aws-cli help
```

#### Mount the current directory and copy a zip file to s3
```
root:~$ docker run --rm -v $HOME/.aws:/root/.aws -v $PWD:/root/data aws-cli s3 cp any.zip s3://SOME_BUCKET/SOME_KEY/any.zip
```

#### Help
The image also allows you to run some various help commands via the docker-entrypoint.sh script. You can view the help by passing the _--docker-help_ option

```
root:~$ docker run -it --rm aws-cli --docker-help
Usage: To run this image you can pass these commands only

  aws curl git less zip jq bash

If you specifiy anything other than those commands then those arguments will be passed to the 'aws' command by default including options

Examples:
  ### runs aws help ###
  docker run -it --rm aws-cli help

  ### Also runs aws help ###
  docker run -it --rm aws-cli aws help

  ### Displays the aws cli version ###
  docker run --rm aws-cli --version

  ### Passes arguments to aws cli calling the elasticbeanstalk subcommand ###
  docker run -it --rm aws-cli elasticbeanstalk help

  ### runs an interactive bash shell ###
  docker run -it --rm aws-cli bash

  ### zips the data dir ###
  docker run --rm aws-cli zip -9 -r pwd.zip .
```

Interactivly with the current directory using a bash shell
```
root:~$ docker run -it --rm -v $HOME/.aws:/root/.aws -v $PWD:/root/data aws-cli bash
```

Alternativley and with less typing just use docker compose with a docker-compose.yml file like
```
aws-cli:
  image: aws-cli:latest
  command: bash
  volumes:
    - $HOME/.aws:/root/.aws:ro
    - ./:/root/data
```
then ...
``` 
docker-compose up
```

## Author
Patrick H. McJury <patrick.mcjury@macmillan.com>